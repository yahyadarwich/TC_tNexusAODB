package Pages;

import Elements.FDM_Insert_Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FDM_Insert_Page extends FDM_Insert_Elements {
  WebDriver webDriver;

  public FDM_Insert_Page(WebDriver webDriver) {
    this.webDriver = webDriver;
  }

  public void clickElement(By webElement) {
    webDriver.findElement(webElement).click();
  }
  public void sendKeys(By webElement, String keys) {
    webDriver.findElement(webElement).sendKeys(keys);
  }

  public String getText(By webElement) {
    return webDriver.findElement(webElement).getText();
  }

  public boolean isDisplayed(By webElement) {
    return webDriver.findElement(webElement).isDisplayed();
  }
}
