package Pages;

import Elements.FDM_MainPage_Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class FDM_Main_Page extends FDM_MainPage_Elements {
  WebDriver webDriver;
  JavascriptExecutor js;
  Actions actions;
  public String downloadsPath = "C:\\Users\\Yahya Darwich\\Downloads";

  public FDM_Main_Page(WebDriver webDriver) {
    this.webDriver = webDriver;
    this.js = (JavascriptExecutor) webDriver;
    this.actions = new Actions(webDriver);
  }

  public void clickOne_Tab(By webElement) {
    js.executeScript("arguments[0].click()", webDriver.findElement(webElement));
  }

  public void clickElement(By webElement) {
    actions.moveToElement(webDriver.findElement(webElement)).perform();
    webDriver.findElement(webElement).click();
  }

  public void rightClickElement(By webElement) {
    actions.contextClick(webDriver.findElement(webElement)).perform();
  }

  public FDM_Insert_Page clickInsert() {
    webDriver.findElement(insert).click();
    return new FDM_Insert_Page(webDriver);
  }

  public String VerifyExpectedFileName() throws InterruptedException {
    Thread.sleep(3000);
    File getLatestFile = getLatestFileFromDir(downloadsPath);
    return getLatestFile.getName();
  }

  public void deleteFile(String path) throws InterruptedException {
    Thread.sleep(3000);
    File file = new File(path);
    file.delete();
  }

  private File getLatestFileFromDir(String dirPath) {
    File dir = new File(dirPath);
    File[] files = dir.listFiles();
    if (files == null || files.length == 0) {
      return null;
    }
    File lastModifiedFile = files[0];
    for (int i = 1; i < files.length; i++) {
      if (lastModifiedFile.lastModified() < files[i].lastModified()) {
        lastModifiedFile = files[i];
      }
    }
    return lastModifiedFile;
  }

  public FDM_Edit_Page doubleClickFDMGrid() {
    webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    Actions actions = new Actions(webDriver);
    WebElement webElement = webDriver.findElement(grid_FDM);
    actions.doubleClick(webElement).perform();
    return new FDM_Edit_Page(webDriver);
  }
}
