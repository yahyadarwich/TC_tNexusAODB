package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class HomePage {
  private WebDriver webDriver;

  public HomePage(WebDriver webDriver) {
    this.webDriver = webDriver;
  }

  public FDM_Main_Page clickFDMLink() {
    clickLink("fdm");
    return new FDM_Main_Page(webDriver);
  }

  public void clickElement(By webElement) {
    webDriver.findElement(webElement).click();
  }

  public String getTimeNow(By webElement) {
    System.out.println("time:" + webDriver.findElement(webElement).getText());
    return webDriver.findElement(webElement).getText();
  }

  public boolean readExportCSV(String utc, String path) {
    try {
      File myObj = new File(path);
      boolean contains = false;
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        if (data.contains(utc)) contains = true;
        else contains = false;
      }
      myReader.close();
      return contains;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return false;
    }
  }

  private void clickLink(String linkText) {
    webDriver.findElement(By.cssSelector("a[href=\"/" + linkText + "\"] > div")).click();
  }
}
