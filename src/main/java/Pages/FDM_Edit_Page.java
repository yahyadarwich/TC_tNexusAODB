package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FDM_Edit_Page {
  WebDriver webDriver;

  public FDM_Edit_Page(WebDriver webDriver) {
    this.webDriver = webDriver;
  }
  public void clickElement(By webElement) {
    webDriver.findElement(webElement).click();
  }

  public void sendKeys(By webElement, String keys) {
    webDriver.findElement(webElement).sendKeys(keys);
  }
  public String getText(By webElement) {
    return webDriver.findElement(webElement).getText();
  }

  public boolean isDisplayed(By webElement) {
    return webDriver.findElement(webElement).isDisplayed();
  }
}
