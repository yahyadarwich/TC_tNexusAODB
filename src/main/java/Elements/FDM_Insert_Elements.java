package Elements;

import org.openqa.selenium.By;

public class FDM_Insert_Elements {

  public final By header = By.cssSelector("button.sc-fEOsli.jnFcnU > span:nth-child(2)");
  public final By saveBtn = By.cssSelector("div.sc-ckMVTt.hcWITW  span.labelClass");
  public final By closeBtn =
      By.cssSelector("header.fdm__module_box .dialogBox__header__close:nth-child(1)");
  public final By arrivalSpan = By.cssSelector(".sc-gSAPjG.hdOgUf.formOpsView > span");
  public final By departureSpan = By.cssSelector(".sc-gSAPjG.gWIWJb.formOpsView > span");
  public final By dep_logo =
      By.cssSelector(
          ".MuiGrid-root.DepartureGridRight.MuiGrid-container.MuiGrid-grid-xs-9 .sc-dIouRR");
  public final By arr_FlightNumber =
      By.cssSelector(
          ".MuiGrid-root.movement__column.MuiGrid-container.MuiGrid-spacing-xs-2.MuiGrid-grid-xs-6:first-of-type .sc-papXJ.kwUXqk");
  public final By first_arr_FlightNumber =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div[1]");
  public final By dep_FlightNumber =
      By.cssSelector(
          ".MuiGrid-root.movement__column.MuiGrid-container.MuiGrid-spacing-xs-2.MuiGrid-grid-xs-6:last-of-type .sc-papXJ.kwUXqk");
  public final By first_dep_FlightNumber =
          By.xpath(
                  "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[1]");
  public final By arr_FlightNumberSub =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[2]/input");
  public final By dep_FlightNumberSub =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/input");
  public final By arr_FlightNumberSuffix =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[1]/div[1]/div/div/div[1]/div/div[3]/input");
  public final By dep_FlightNumberSuffix =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[3]/input");
  public final By arr_ADEP =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[2]/div/div/div[1]/div[2]/input");
  public final By first_arr_ADEP =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[2]/div/div/div[1]/div[2]/div/div[2]/div/div/div[1]");
  public final By dep_ADEP =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[2]/div/div/div[1]/div[2]/input");
  public final By first_dep_ADEP =
          By.xpath(
                  "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[2]/div/div/div[1]");
  public final By arr_Category =
      By.cssSelector(
          "body > div.MuiDialog-root.small_window.flight_details.fdm-component.dynamic_rowHeight > div.MuiDialog-container.MuiDialog-scrollPaper > div > div > div > form > div.sc-gITdmR.gUayCS > div > div:nth-child(1) > div.MuiGrid-root.moment_arrival.fdm_tbx-formBody.MuiGrid-container.MuiGrid-spacing-xs-2 > div > div > div:nth-child(5) > div.sc-hHLeRK.gjGSvJ.DropDownWrapper > input");
  public final By first_arr_Category =
      By.cssSelector(
          "body > div.MuiDialog-root.small_window.flight_details.fdm-component.dynamic_rowHeight > div.MuiDialog-container.MuiDialog-scrollPaper > div > div > div > form > div.sc-gITdmR.gUayCS > div > div:nth-child(1) > div.MuiGrid-root.moment_arrival.fdm_tbx-formBody.MuiGrid-container.MuiGrid-spacing-xs-2 > div > div > div:nth-child(5) > div.sc-hHLeRK.fUkWob.DropDownWrapper > div > div.sc-llJcti.jXqlUh.dropdown_tBody > div > div > div:nth-child(1)");
  public final By first_dep_Category =
          By.xpath(
                  "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[2]/div/div/div[6]/div[2]/div/div[2]/div/div/div[1]");
  public final By dep_Category =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[2]/div/div/div[6]/div[2]/input");
  public final By arr_SIBT =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[2]/div/div/div[6]/div[2]/input");
  public final By dep_SIBT =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[2]/div/div/div[4]/div[2]/input");
  public final By ac_type =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[1]/div/div/div/div[2]");
  public final By first_ac_type =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div[1]");
  public final By arr_CallSign =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[1]/div[1]/div/div/div[3]/div/div/input");
  public final By dep_CallSign =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[2]/div[1]/div[2]/div/div/div[4]/div/div/input");
  public final By registration =
      By.xpath(
          "/html/body/div[9]/div[3]/div/div/div/form/div[2]/div/div[1]/div[1]/div[2]/div/div/div/div[2]/input");
  public final By yesBtn = By.xpath("/html/body/div[14]/div[3]/div/div[2]/button[2]");
  public final By errorMsg = By.cssSelector(".sc-fbPSWO.fRAQgw");
}
