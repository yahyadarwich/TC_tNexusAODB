package Elements;

import org.openqa.selenium.By;

public class FDM_Edit_Elements {
    public final By editBtn = By.xpath("/html/body/div[1]/div/div[1]/div[5]/div/div/div[3]/div/header/div[2]/button");
    public final By saveBtn = By.xpath("/html/body/div[1]/div/div[1]/div[5]/div/div/div[3]/div/header/div[2]/div");

    public final By dep_AOBT = By.xpath("/html/body/div[1]/div/div[1]/div[5]/div/div/div[3]/div/div/div/form/div/div/div[2]/div[2]/div/div[1]/div[4]/div[5]/div[2]/input");

    public final By dep_status = By.xpath("/html/body/div[1]/div/div[1]/div[5]/div/div/div[3]/div/div/div/form/div/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/div[2]/input");

    public final By GRT_option = By.xpath("/html/body/div[1]/div/div[1]/div[5]/div/div/div[3]/div/div/div/form/div/div/div[2]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/div/div[3]");
    public final By yesBtn = By.xpath("/html/body/div[27]/div[3]/div/div[2]/button[2]");
}
