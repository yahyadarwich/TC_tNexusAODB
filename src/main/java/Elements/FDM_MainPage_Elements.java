package Elements;

import org.openqa.selenium.By;

public class FDM_MainPage_Elements {
  public final By fdmTabs = By.xpath("//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div/div[2]");
  public final By fdm_arr_Tab =
      By.cssSelector("ul .p-dropdown-item.p-highlight[aria-label=\"Arrival \"]");
  public final By fdm_dep_Tab =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div/div[2]/div/div/div/div[4]/div/ul/li[2]");
  public final By fdm_turn_Tab =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div/div[2]/div/div/div/div[4]/div/ul/li[3]");
  public final By fdm_aircraft_Tab =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div/div[2]/div/div/div/div[4]/div/ul/li[4]");
  public final By fdm_additionalFlights_Tab =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div/div[2]/div/div/div/div[4]/div/ul/li[5]");
  public final By movementIcon =
      By.cssSelector("button.MuiButtonBase-root.MuiIconButton-root.grid_extended_btn");
  public final By insert =
      By.cssSelector("ul.MuiList-root.Grid_extended_menu.MuiList-padding li:nth-child(1)");

  public final By grid_FDM =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/section/div/div[1]/div/div[2]/div[2]/div[3]/div[1]/div[2]");

  public final By tows_grid =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div[2]/div/div/div/section/div/div[2]/div/div[2]/div[2]/div[3]/div[2]/div/div/div[1]");

  public final By export_menu_element =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div[2]/div/div/div/section/div/div[2]/div/div[6]/div/div/div[5]");
  public final By csv_export_menu_element =
      By.xpath(
          "//*[@id=\"root\"]/div/div[1]/div[5]/div/section/div[2]/div/div/div/section/div/div[2]/div/div[7]/div/div/div[2]/span[2]");
}
