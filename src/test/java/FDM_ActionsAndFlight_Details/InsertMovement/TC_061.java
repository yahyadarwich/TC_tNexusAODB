package FDM_ActionsAndFlight_Details.InsertMovement;

import Elements.FDM_Insert_Elements;
import Elements.FDM_MainPage_Elements;
import Pages.FDM_Insert_Page;
import Pages.FDM_Main_Page;
import SetupEnv.Setup;
import org.junit.jupiter.api.Test;

public class TC_061 extends Setup {
    @Test
    public void TC_061(){
        FDM_Main_Page fdm_main_page = homePage.clickFDMLink();
        FDM_MainPage_Elements fdm_mainPage_elements = new FDM_MainPage_Elements();
        fdm_main_page.clickElement(fdm_mainPage_elements.fdmTabs);
        fdm_main_page.clickOne_Tab(fdm_mainPage_elements.fdm_arr_Tab);
        fdm_main_page.clickElement(fdm_mainPage_elements.movementIcon);
        FDM_Insert_Page fdm_insert_page = fdm_main_page.clickInsert();
        FDM_Insert_Elements fdm_insert_elements = new FDM_Insert_Elements();
        fdm_insert_page.clickElement(fdm_insert_elements.dep_ADEP);
        fdm_insert_page.clickElement(fdm_insert_elements.first_dep_ADEP);
        fdm_insert_page.clickElement(fdm_insert_elements.dep_Category);
        fdm_insert_page.clickElement(fdm_insert_elements.first_dep_Category);
        fdm_insert_page.clickElement(fdm_insert_elements.dep_FlightNumber);
        fdm_insert_page.clickElement(fdm_insert_elements.first_dep_FlightNumber);
        fdm_insert_page.clickElement(fdm_insert_elements.ac_type);
        fdm_insert_page.clickElement(fdm_insert_elements.first_ac_type);
        fdm_insert_page.sendKeys(fdm_insert_elements.dep_FlightNumberSub, "1");
        fdm_insert_page.clickElement(fdm_insert_elements.saveBtn);
        fdm_insert_page.clickElement(fdm_insert_elements.yesBtn);
    }
}
