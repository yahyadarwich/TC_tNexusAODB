package FDM_ActionsAndFlight_Details.InsertMovement;

import Elements.FDM_Edit_Elements;
import Elements.FDM_MainPage_Elements;
import Pages.FDM_Edit_Page;
import Pages.FDM_Main_Page;
import SetupEnv.Setup;
import org.junit.jupiter.api.Test;

public class TC_226 extends Setup {
    @Test
    public void TC_226(){
        FDM_Main_Page fdm_main_page = homePage.clickFDMLink();
        FDM_MainPage_Elements fdm_mainPage_elements = new FDM_MainPage_Elements();
        fdm_main_page.clickElement(fdm_mainPage_elements.fdmTabs);
        fdm_main_page.clickOne_Tab(fdm_mainPage_elements.fdm_dep_Tab);
        FDM_Edit_Page fdm_edit_page = fdm_main_page.doubleClickFDMGrid();
        FDM_Edit_Elements fdm_edit_elements = new FDM_Edit_Elements();
        fdm_edit_page.clickElement(fdm_edit_elements.editBtn);
        fdm_edit_page.sendKeys(fdm_edit_elements.dep_AOBT, "20:00");
        fdm_edit_page.clickElement(fdm_edit_elements.dep_status);
        fdm_edit_page.clickElement(fdm_edit_elements.GRT_option);
        fdm_edit_page.clickElement(fdm_edit_elements.saveBtn);
        fdm_edit_page.clickElement(fdm_edit_elements.yesBtn);
    }
}
