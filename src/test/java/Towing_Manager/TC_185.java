package Towing_Manager;

import Elements.FDM_MainPage_Elements;
import Elements.Public_Elements;
import Pages.FDM_Main_Page;
import SetupEnv.Setup;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TC_185 extends Setup {
  @Test
  public void TC_185() throws InterruptedException {
    Public_Elements public_elements = new Public_Elements();
    homePage.clickElement(public_elements.time_now);
    assertTrue(
        homePage.getTimeNow(public_elements.time_now).contains("UTC"), "Time not of format UTC");
    FDM_Main_Page fdm_main_page = homePage.clickFDMLink();
    FDM_MainPage_Elements fdm_mainPage_elements = new FDM_MainPage_Elements();
    fdm_main_page.clickElement(fdm_mainPage_elements.fdmTabs);
    Thread.sleep(3000);
    fdm_main_page.clickOne_Tab(fdm_mainPage_elements.fdm_aircraft_Tab);
    fdm_main_page.rightClickElement(fdm_mainPage_elements.tows_grid);
    fdm_main_page.clickElement(fdm_mainPage_elements.export_menu_element);
    fdm_main_page.clickElement(fdm_mainPage_elements.csv_export_menu_element);
    assertTrue(
        fdm_main_page.VerifyExpectedFileName().equals("export.csv"),
        "Downloaded file name is not matching with expected file name");
    assertTrue(
        homePage.readExportCSV("00:00:00", "C:\\Users\\Yahya Darwich\\Downloads\\export.csv"),
        "Error in date format");
    fdm_main_page.deleteFile("C:\\Users\\Yahya Darwich\\Downloads\\export.csv");
  }
}
