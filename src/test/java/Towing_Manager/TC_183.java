package Towing_Manager;

import Elements.FDM_MainPage_Elements;
import Pages.FDM_Main_Page;
import SetupEnv.Setup;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TC_183 extends Setup {
  @Test
  public void TC_183() throws InterruptedException {
    FDM_Main_Page fdm_main_page = homePage.clickFDMLink();
    FDM_MainPage_Elements fdm_mainPage_elements = new FDM_MainPage_Elements();
    fdm_main_page.clickElement(fdm_mainPage_elements.fdmTabs);
    Thread.sleep(3000);
    fdm_main_page.clickOne_Tab(fdm_mainPage_elements.fdm_aircraft_Tab);
    fdm_main_page.rightClickElement(fdm_mainPage_elements.tows_grid);
    fdm_main_page.clickElement(fdm_mainPage_elements.export_menu_element);
    fdm_main_page.clickElement(fdm_mainPage_elements.csv_export_menu_element);
    assertTrue(
        fdm_main_page.VerifyExpectedFileName().equals("export.csv"),
        "Downloaded file name is not matching with expected file name");
    fdm_main_page.deleteFile("C:\\Users\\Yahya Darwich\\Downloads\\export.csv");
  }
}
